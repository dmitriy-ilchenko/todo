package com.example.todo.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.todo.App
import com.example.todo.R
import com.example.todo.adapter.TabPagerAdapter
import com.example.todo.dialog.AddTaskDialog
import com.example.todo.dialog.DeleteAllTasksDialog
import com.example.todo.dialog.ErrorDialog
import com.example.todo.fragment.TaskListFragment
import com.example.todo.model.TaskFilterType
import com.example.todo.viewmodel.MainViewModel
import com.example.todo.viewmodel.MainViewModelFactory
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), DeleteAllTasksDialog.Listener, AddTaskDialog.Listener {
    private val viewModelDisposable = CompositeDisposable()
    private lateinit var viewModel: MainViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initToolbar()
        initTabs()
        initEventHandlers()
        createViewModel()
    }

    override fun onStart() {
        super.onStart()
        subscribeToViewModel()
    }

    override fun onStop() {
        super.onStop()
        unsubscribeFromViewModel()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_activity_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.item_delete_all) {
            viewModel.handleDeleteAllTasksClicked()
            return true
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onDeleteAllTasks() {
        viewModel.handleDeleteAllTasksConfirmed()
    }

    override fun onAddTask(text: String) {
        viewModel.handleAddTaskConfirmed(text)
    }


    private fun initToolbar() {
        setSupportActionBar(toolbar)
    }

    private fun initTabs() {
        val tabFragments = arrayOf<Fragment>(
            TaskListFragment.newInstance(TaskFilterType.ALL),
            TaskListFragment.newInstance(TaskFilterType.ACTIVE),
            TaskListFragment.newInstance(TaskFilterType.COMPLETED)
        )
        val titles = resources.getStringArray(R.array.tab_titles)

        view_pager.adapter = TabPagerAdapter(supportFragmentManager, tabFragments, titles)
        tab_layout.setupWithViewPager(view_pager)
    }

    private fun initEventHandlers() {
        add_task_button.setOnClickListener { viewModel.handleAddTaskClicked() }
    }


    private fun createViewModel() {
        val taskRepository = (application as? App)?.taskRepository ?: return
        val viewModelFactory = MainViewModelFactory(taskRepository)
        viewModel = ViewModelProviders
            .of(this, viewModelFactory)
            .get(MainViewModel::class.java)
    }

    private fun subscribeToViewModel() {
        viewModel.isLoading
            .subscribe { showIsLoading(it) }
            .addTo(viewModelDisposable)

        viewModel.eventShowDeleteAllTasksDialog
            .subscribe { showDeleteAllTasksDialog() }
            .addTo(viewModelDisposable)

        viewModel.eventShowAddTasksDialog
            .subscribe { showAddTaskDialog() }
            .addTo(viewModelDisposable)

        viewModel.error
            .subscribe { showErrorDialog() }
            .addTo(viewModelDisposable)
    }

    private fun unsubscribeFromViewModel() {
        viewModelDisposable.clear()
    }


    private fun showIsLoading(isLoading: Boolean) {
        loading_progress_bar.isVisible = isLoading
        tab_layout.isVisible = !isLoading
        add_task_button.isVisible = !isLoading
    }

    private fun showDeleteAllTasksDialog() {
        val dialog = DeleteAllTasksDialog()
        dialog.show(supportFragmentManager, "DeleteAllTasksDialog")
    }

    private fun showAddTaskDialog() {
        val dialog = AddTaskDialog()
        dialog.show(supportFragmentManager, "AddTaskDialog")
    }

    private fun showErrorDialog() {
        val dialog = ErrorDialog()
        dialog.show(supportFragmentManager, "ErrorDialog")
    }
}
