package com.example.todo.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.todo.model.Task
import com.example.todo.repository.TaskRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject


class MainViewModel(private val taskRepository: TaskRepository) : ViewModel() {
    private val disposable = CompositeDisposable()

    val isLoading: Subject<Boolean> = BehaviorSubject.create()
    val error: Subject<Throwable> = PublishSubject.create()
    val eventShowDeleteAllTasksDialog: Subject<Unit> = PublishSubject.create()
    val eventShowAddTasksDialog: Subject<Unit> = PublishSubject.create()

    fun handleDeleteAllTasksClicked() {
        eventShowDeleteAllTasksDialog.onNext(Unit)
    }

    fun handleAddTaskClicked() {
        eventShowAddTasksDialog.onNext(Unit)
    }

    fun handleDeleteAllTasksConfirmed() {
        taskRepository.deleteAll()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { isLoading.onNext(true) }
            .doOnTerminate { isLoading.onNext(false) }
            .subscribe(
                { },
                { error -> this.error.onNext(error) }
            )
            .addTo(disposable)
    }

    fun handleAddTaskConfirmed(text: String) {
        if (text.isEmpty()) {
            return
        }

        val task = Task(
            isCompleted = false,
            text = text
        )

        taskRepository.insert(task)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { isLoading.onNext(true) }
            .doOnTerminate { isLoading.onNext(false) }
            .subscribe(
                { },
                { error -> this.error.onNext(error) }
            )
            .addTo(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}


class MainViewModelFactory(private val taskRepository: TaskRepository) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel(taskRepository) as T
    }
}