package com.example.todo.viewmodel

import androidx.lifecycle.ViewModel
import com.example.todo.repository.TaskRepository
import io.reactivex.BackpressureStrategy
import androidx.paging.RxPagedListBuilder
import com.example.todo.model.Task
import android.nfc.tech.MifareUltralight.PAGE_SIZE
import androidx.lifecycle.ViewModelProvider
import androidx.paging.DataSource
import androidx.paging.PagedList
import com.example.todo.model.TaskFilterType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject


class TaskListViewModel(
    private val taskRepository: TaskRepository,
    private val taskFilterType: TaskFilterType
) : ViewModel() {

    private val disposable = CompositeDisposable()

    val tasks: Subject<PagedList<Task>> = BehaviorSubject.create()
    val error: Subject<Throwable> = PublishSubject.create()

    init {
        loadTasks()
    }

    fun handleTaskStatusChanged(isCompleted: Boolean, task: Task) {
        if (task.isCompleted == isCompleted) {
            return
        }

        task.isCompleted = isCompleted

        taskRepository.update(task)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { },
                { error -> this.error.onNext(error) }
            )
            .addTo(disposable)
    }

    fun handleDeleteTaskClicked(task: Task) {
        taskRepository.delete(task)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { },
                { error -> this.error.onNext(error) }
            )
            .addTo(disposable)
    }

    private fun loadTasks() {
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(PAGE_SIZE)
            .build()

        val dataSourceFactory: DataSource.Factory<Int, Task> = when (taskFilterType) {
            TaskFilterType.ALL -> taskRepository.getAllTasks()
            TaskFilterType.ACTIVE -> taskRepository.getActiveTasks()
            TaskFilterType.COMPLETED -> taskRepository.getCompletedTasks()
        }

        RxPagedListBuilder<Int, Task>(dataSourceFactory, config)
            .buildFlowable(BackpressureStrategy.LATEST)
            .subscribe(
                { taskList -> tasks.onNext(taskList) },
                { error -> this.error.onNext(error) }
            )
            .addTo(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}


class TaskListViewModelFactory(
    private val taskRepository: TaskRepository,
    private val taskFilterType: TaskFilterType
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TaskListViewModel(taskRepository, taskFilterType) as T
    }
}