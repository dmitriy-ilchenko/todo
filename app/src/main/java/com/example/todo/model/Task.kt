package com.example.todo.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Task(
    @PrimaryKey(autoGenerate = true) var id: Long = 0L,
    var isCompleted: Boolean,
    var text: String
)