package com.example.todo.model

enum class TaskFilterType {
    ALL,
    ACTIVE,
    COMPLETED,
}

fun TaskFilterType.toInt(): Int {
    return ordinal
}

fun Int.toTaskFilterType(): TaskFilterType {
    return TaskFilterType.values()[this]
}