package com.example.todo

import android.app.Application
import com.example.todo.repository.TaskRepository
import com.example.todo.repository.db.DbTaskRepository
import androidx.room.Room
import com.example.todo.repository.db.TaskDb


class App : Application() {
    lateinit var taskRepository: TaskRepository

    override fun onCreate() {
        super.onCreate()
        taskRepository = createTaskRepository()
    }

    private fun createTaskRepository(): TaskRepository {
        val database = Room.databaseBuilder(this, TaskDb::class.java, "db").build()
        val dao = database.getTaskDao()
        return DbTaskRepository(dao)
    }
}