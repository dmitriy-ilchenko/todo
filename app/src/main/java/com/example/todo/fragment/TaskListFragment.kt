package com.example.todo.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.todo.App
import com.example.todo.R
import com.example.todo.model.Task
import com.example.todo.model.TaskFilterType
import com.example.todo.adapter.TaskListAdapter
import com.example.todo.dialog.ErrorDialog
import com.example.todo.model.toInt
import com.example.todo.model.toTaskFilterType
import com.example.todo.viewmodel.TaskListViewModel
import com.example.todo.viewmodel.TaskListViewModelFactory
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_task_list.*


private const val filterTypeArgumentsKey = "filterTypeArgumentsKey"


class TaskListFragment : Fragment(), TaskListAdapter.Listener {

    companion object {
        fun newInstance(filterType: TaskFilterType): TaskListFragment {
            val arguments = Bundle()
            arguments.putInt(filterTypeArgumentsKey, filterType.toInt())

            val fragment = TaskListFragment()
            fragment.arguments = arguments
            return fragment
        }
    }


    private val adapter = TaskListAdapter(this)
    private val viewModelDisposable = CompositeDisposable()
    private lateinit var viewModel: TaskListViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createViewModel()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_task_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
    }

    override fun onStart() {
        super.onStart()
        subscribeToViewModel()
    }

    override fun onStop() {
        super.onStop()
        unsubscribeFromViewModel()
    }


    override fun onTaskStatusChanged(isCompleted: Boolean, task: Task) {
        viewModel.handleTaskStatusChanged(isCompleted, task)
    }

    override fun onDeleteTask(task: Task) {
        viewModel.handleDeleteTaskClicked(task)
    }


    private fun initRecyclerView() {
        task_list_recycler_view.layoutManager = LinearLayoutManager(requireContext())
        task_list_recycler_view.adapter = adapter
    }


    private fun createViewModel() {
        val taskRepository = (requireActivity().application as? App)?.taskRepository ?: return
        val taskFilterType = arguments?.getInt(filterTypeArgumentsKey)?.toTaskFilterType() ?: return
        val viewModelFactory = TaskListViewModelFactory(taskRepository, taskFilterType)
        viewModel = ViewModelProviders
            .of(this, viewModelFactory)
            .get(TaskListViewModel::class.java)
    }

    private fun subscribeToViewModel() {
        viewModel.tasks
            .subscribe { showTasks(it) }
            .addTo(viewModelDisposable)

        viewModel.error
            .subscribe { showErrorDialog() }
            .addTo(viewModelDisposable)
    }

    private fun unsubscribeFromViewModel() {
        viewModelDisposable.clear()
    }


    private fun showTasks(tasks: PagedList<Task>) {
        adapter.submitList(tasks)
    }

    private fun showErrorDialog() {
        val dialog = ErrorDialog()
        dialog.show(childFragmentManager, "ErrorDialog")
    }
}