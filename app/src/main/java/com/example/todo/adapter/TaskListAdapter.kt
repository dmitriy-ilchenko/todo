package com.example.todo.adapter

import android.graphics.Paint
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.DiffUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import com.example.todo.R
import com.example.todo.model.Task
import kotlinx.android.synthetic.main.item_task_list.view.*


class TaskListAdapter(private val listener: Listener) :
    PagedListAdapter<Task, TaskListAdapter.ViewHolder>(TaskDiffUtilItemCallback()) {


    interface Listener {
        fun onTaskStatusChanged(isCompleted: Boolean, task: Task)
        fun onDeleteTask(task: Task)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_task_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val task = getItem(position)
        if (task != null) {
            holder.bind(task)
        } else {
            holder.clear()
        }
    }


    private class TaskDiffUtilItemCallback : DiffUtil.ItemCallback<Task>() {
        override fun areItemsTheSame(oldItem: Task, newItem: Task): Boolean {
            return oldItem === newItem || oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Task, newItem: Task): Boolean {
            return oldItem == newItem
        }
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(task: Task) {
            itemView.completed_check_box.setOnCheckedChangeListener { _, isChecked ->
                listener.onTaskStatusChanged(isChecked, task)
                setTextStrikethrough(isChecked)
            }

            itemView.delete_button.setOnClickListener { listener.onDeleteTask(task) }

            itemView.completed_check_box.isChecked = task.isCompleted
            itemView.task_text_view.text = task.text
        }

        fun clear() {
            itemView.invalidate()
        }

        private fun setTextStrikethrough(isStrikethrough: Boolean) {
            var paintFlags = itemView.task_text_view.paintFlags

            paintFlags = if (isStrikethrough) {
                paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            } else {
                paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
            }

            itemView.task_text_view.paintFlags = paintFlags
        }
    }
}
