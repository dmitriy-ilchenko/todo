package com.example.todo.repository.db

import androidx.paging.DataSource
import com.example.todo.model.Task
import com.example.todo.repository.TaskRepository
import io.reactivex.Completable


class DbTaskRepository(private val taskDao: TaskDao) : TaskRepository {

    override fun getAllTasks(): DataSource.Factory<Int, Task> {
        return taskDao.getAllTasks()
    }

    override fun getActiveTasks(): DataSource.Factory<Int, Task> {
        return taskDao.getActiveTasks()
    }

    override fun getCompletedTasks(): DataSource.Factory<Int, Task> {
        return taskDao.getCompletedTasks()
    }

    override fun insert(task: Task): Completable {
        return Completable.fromAction { taskDao.insert(task) }
    }

    override fun update(task: Task): Completable {
        return Completable.fromAction { taskDao.update(task) }
    }

    override fun delete(task: Task): Completable {
        return Completable.fromAction { taskDao.delete(task) }
    }

    override fun deleteAll(): Completable {
        return Completable.fromAction { taskDao.deleteAll() }
    }
}