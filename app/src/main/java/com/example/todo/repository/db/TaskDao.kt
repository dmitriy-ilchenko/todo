package com.example.todo.repository.db

import androidx.paging.DataSource
import androidx.room.*
import com.example.todo.model.Task

@Dao
interface TaskDao {
    @Query("SELECT * FROM task")
    fun getAllTasks(): DataSource.Factory<Int, Task>

    @Query("SELECT * FROM task WHERE isCompleted = 0")
    fun getActiveTasks(): DataSource.Factory<Int, Task>

    @Query("SELECT * FROM task WHERE isCompleted = 1")
    fun getCompletedTasks(): DataSource.Factory<Int, Task>

    @Insert
    fun insert(task: Task)

    @Update
    fun update(task: Task)

    @Delete
    fun delete(task: Task)

    @Query("DELETE FROM task")
    fun deleteAll()
}