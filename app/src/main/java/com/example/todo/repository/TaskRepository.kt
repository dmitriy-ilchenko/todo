package com.example.todo.repository

import androidx.paging.DataSource
import io.reactivex.Completable
import com.example.todo.model.Task

interface TaskRepository {
    fun getAllTasks(): DataSource.Factory<Int, Task>
    fun getActiveTasks(): DataSource.Factory<Int, Task>
    fun getCompletedTasks(): DataSource.Factory<Int, Task>
    fun insert(task: Task): Completable
    fun update(task: Task): Completable
    fun delete(task: Task): Completable
    fun deleteAll(): Completable
}