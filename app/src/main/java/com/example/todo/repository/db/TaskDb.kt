package com.example.todo.repository.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.todo.model.Task

@Database(entities = [Task::class], version = 1)
abstract class TaskDb : RoomDatabase() {
    abstract fun getTaskDao(): TaskDao
}