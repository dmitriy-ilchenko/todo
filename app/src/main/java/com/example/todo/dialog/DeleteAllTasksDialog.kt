package com.example.todo.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.example.todo.R


class DeleteAllTasksDialog : DialogFragment() {

    interface Listener {
        fun onDeleteAllTasks()
    }

    private lateinit var listener: Listener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as Listener
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(requireContext())
            .setTitle(R.string.delete_all_tasks_dialog_title)
            .setMessage(R.string.delete_all_tasks_dialog_message)
            .setPositiveButton(R.string.delete_all_tasks_dialog_positive_button) { _, _ ->
                listener.onDeleteAllTasks()
            }
            .setNegativeButton(R.string.delete_all_tasks_dialog_negative_button, null)
            .create()
    }
}