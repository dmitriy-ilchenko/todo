package com.example.todo.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import androidx.fragment.app.DialogFragment
import androidx.appcompat.app.AlertDialog
import com.example.todo.R
import kotlinx.android.synthetic.main.dialog_add_task.view.*

class AddTaskDialog : DialogFragment() {

    interface Listener {
        fun onAddTask(text: String)
    }


    private lateinit var listener: Listener


    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as Listener
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val inflater = LayoutInflater.from(requireContext())

        val dialogView = inflater.inflate(R.layout.dialog_add_task, null, false)
        val taskEditText = dialogView.task_edit_text

        return AlertDialog.Builder(requireContext())
            .setTitle(R.string.add_task_dialog_title)
            .setView(dialogView)
            .setPositiveButton(R.string.delete_all_tasks_dialog_positive_button) { _, _ ->
                val taskText = taskEditText.text.toString()
                listener.onAddTask(taskText)
            }
            .setNegativeButton(R.string.delete_all_tasks_dialog_negative_button, null)
            .create()
    }
}